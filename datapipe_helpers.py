import boto3
import re
import bs4
import pandas as pd

#add word breaks if too many false pos... or make spaces/dashes a requirement
regexes = {
    r'\b([a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+)\b': 'email', 
    r'\b(?!000|666)[0-8][0-9]{2}(-| )(?!00)[0-9]{2}(-| )(?!0000)[0-9]{4}\b': 'ssn-US',
    r'((?:\d{1,2}\-)?(?:(?:\d{3})|(?:\(\d{3}\)))(?:[-\s\.])?(?:\d{3})(?:[-\s\.])(?:\d{4}))': 'phone-US',
    r'(?:^| |:|\|)(?:[0-9]{2}[0,1,5][0-9][0-9]{2}/[0-9]{4})\b':'Czech,Slovakia - Birth Number',
    r'\b[A-Z]{2}[0-9]{6}\b':'Czech,Slovakia - COP',
    r'(?:^| |:|\|)[0-9]{2}[0,1][0-9][0-9]{2}-[0-9]{4}\b':'Denmark',
    r'\b[A-Z]{2}?[ ]?[0-9]{2}[ ]?[0-9]{4}[ ][0-9]{4}[ ][0-9]{4}[ ][0-9]{4}[ ][0-9]{4}\b':'Europe',
    r'\b[1,2][ ][0-9]{2}[ ][0,1,2,3,5][0-9][ ][0-9A-Z]{5}[ ][0-9]{3}[ ][0-9]{2}\b':'France',
    r'\b[0-9]{2}[0,1][0-9][0-9]{2}-[A-Z]-[0-9]{5}\b':'Germany-PK',
    r'\b[0-9]{3}/[0-9]{4}/[0-9]{4}\b':'Germany-Steuer-ID',
    r'\b[0-9]{2}[0-9]{2}[0,1][0-9][0-9]{2}[A-Z][0-9]{2}[0-9]\b':'Germany-VSNR,RVNR',
    r'\b[A-Z][ -]?[0-9]{6}\b':'Greece',
    r'\b[1-8][ ][0-9]{2}[0,1][0-9][0-9]{2}[ ][0-9]{4}\b':'Hungary-Szam',
    r'\b[0-9]{7}[A-Z]W?\b':'Ireland',
    r'\b[A-Z]{6}[0-9]{2}[A-E,H,L,M,P,R-T][0-9]{2}[A-Z0-9]{5}\b':'Italy',
    r'\b[0-9]{2}[0,1][0-9][0-9]-[0-9]{5}\b':'Latvia',
    r'\b[0-9,X,M,L,K,Y][0-9]{7}[-][A-Z]\b':'Spain',
    r'(?:^| |:|\|)[0-9]{2}[0-1][0-9][0-9]{2}[-+][0-9]{4}\b':'Sweden',
    r'\b756\.[0-9]{4}\.[0-9]{4}\.[0-9]{2}\b':'Switzerland-2008',
    r'(?:^| |:|\|)[A-CEGHJ-PR-TW-Z][A-CEGHJ-NPR-TW-Z]{1}[0-9]{6}[A-DFM]?\b':'UK-NI',
    r'(?:^| |:|\|)[0-3][0-9]\.?[0,1][0-9]\.?[0-9]{2}[-+A][0-9]{3}[0-9A-Z]\b':'Finland',
    r'\b[0-9]{2}\.(0[1-9]|1[0-2])\.(0[1-9]|1[0-9]|2[0-9]|3[0-1])-[0-9]{3}\.[0-9]{2}\b':'Belgium',
    r'\b[0-9]{3}\.[0-9]{3}\.[0-9]{3}-[0-9]{2}\b':'Brazil-CPF'
}        



tags_for_ids = ['ssn-US', 'Belgium', 'Czech,Slovakia - Birth Number', \
                'Denmark', 'France', 'Germany-Steuer-ID','Hungary-Szam', \
                'Latvia', 'Sweden', 'Switzerland','Switzerland-2008','Brazil-CPF', \
               'Finland', 'Austria-ssPIN', 'Czech,Slovakia - COP', 'Europe', \
                'Germany-PK', 'Germany-VSNR,RVNR', 'Greece', 'Ireland','Italy', \
                'Spain', 'UK-NI']
tags_for_phones = ['phone-US']

def parse_pastebinapi_url(x):
    return x['url'].lower().strip().replace('api_scrape_item.php?i=','')

def get_full_text(x, datapipe):
    response = datapipe.get_response(x['task_id']).body
    soup = bs4.BeautifulSoup(response, 'html.parser')
    text = ' '.join(soup.findAll(text=True))
    return text

def build_keywords_cols(x, keywords):
    tags = []
    for keyword in keywords:
        if keyword in x['text'].lower():
            tags.extend([keyword])
    return pd.Series({'keywords':tags, 'keywords#':len(tags)})

def build_pii_cols(x):
    pii = gen_extra_shingles(x['text'])
    emails, ids, phones = sort_pii(pii)    
    return pd.Series({'email#':len(emails), 'ids#':len(ids), 'phone##':len(phones)})

def is_valid_email(addr):  # noqa
    addr_parts = addr.split('@', 2)
    if len(addr_parts) != 2:
        return False
    name, host = addr_parts
    host_parts = host.split('.')
    if len(host_parts) < 2:
        return False
    if name in ['-', '+']:
        return False
    return True

def find_matches(text, regex):
    """Find all matches of 'regex' in 'text'
    Args:
        text (str): The text to search in.
        regex (regex): regex to match against.
    Yields:
        (str): All matches
    """
    current_pos = 0
    while current_pos <= len(text):
        match = re.compile(regex).search(text[current_pos:])
        if match is None:
            break
        current_pos += match.end()
        yield match.group(0)

def gen_extra_shingles(parsed_text):
    """Extra shingles are special shingles generated if a special case is
    detected.
    Args:
        parsed_text (str): The parsed page contents.
    Returns:
        extra_shingles (list): The extra shingles in plaintext
    """
    extra_shingles = []
    
    for pattern in regexes:
        for match in find_matches(parsed_text, pattern):
            tag = regexes[pattern]
            if tag in ('email',):
                if is_valid_email(match):
                    extra_shingles.append((tag, match.upper()))
            else:
                extra_shingles.append((tag, re.sub('(-| |\.|/|\(|\))', '', match)))              
    return extra_shingles

def sort_pii(pii):
    ids = []
    phones = []
    emails = []
    for item in pii:
        tag = item[0]
        if tag in ('email',):
            emails.append(item[1])
        elif tag in tags_for_ids:
            ids.append(item)
        elif tag in tags_for_phones:
            phones.append(item)
    emails = dedupe(emails) 
    ids = dedupe(ids)
    phones = dedupe(phones) 
    return emails, ids, phones

def dedupe(x):
    return list(set(x))

def read_list_from_file(filename):
    with open(filename,'r') as f:
        lines = f.read().splitlines()
    return lines

def get_breaches(email):
    emailhash = hashlib.sha1(email).hexdigest().upper()
    breaches = []
    filenames = [f for f in os.listdir('./resources/MoreHashedBreaches') if not f.startswith('.')]
    for filename in filenames:
        temp = read_list_from_file('./resources/MoreHashedBreaches/'+filename)
        if emailhash in temp:
            breaches.append(filename[:-4])
    return breaches


# https://alexwlchan.net/2018/01/listing-s3-keys-redux/
def get_matching_s3_objects(bucket, prefix='', suffix=''):
    """
    Generate objects in an S3 bucket.

    :param bucket: Name of the S3 bucket.
    :param prefix: Only fetch objects whose key starts with
        this prefix (optional).
    :param suffix: Only fetch objects whose keys end with
        this suffix (optional).
    """
    s3 = boto3.client('s3')
    kwargs = {'Bucket': bucket}

    # If the prefix is a single string (not a tuple of strings), we can
    # do the filtering directly in the S3 API.
    if isinstance(prefix, str):
        kwargs['Prefix'] = prefix

    while True:

        # The S3 API response is a large blob of metadata.
        # 'Contents' contains information about the listed objects.
        resp = s3.list_objects_v2(**kwargs)

        try:
            contents = resp['Contents']
        except KeyError:
            return

        for obj in contents:
            key = obj['Key']
            if key.startswith(prefix) and key.endswith(suffix):
                yield obj

        # The S3 API is paginated, returning up to 1000 keys at a time.
        # Pass the continuation token into the next response, until we
        # reach the final page (when this field is missing).
        try:
            kwargs['ContinuationToken'] = resp['NextContinuationToken']
        except KeyError:
            break


def get_matching_s3_keys(bucket, prefix='', suffix=''):
    """
    Generate the keys in an S3 bucket.

    :param bucket: Name of the S3 bucket.
    :param prefix: Only fetch keys that start with this prefix (optional).
    :param suffix: Only fetch keys that end with this suffix (optional).
    """
    for obj in get_matching_s3_objects(bucket, prefix, suffix):
        yield obj['Key']