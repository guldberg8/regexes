import boto3
import re
import bs4
import pandas as pd
from googletrans import Translator

#add word breaks if too many false pos... or make spaces/dashes a requirement
regexes = {
    r'\b([a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+)\b': 'email', 
    
    
    #Accounts for labeling words
    #Example: Email : gmail@wkwkland123.com |>> Password : eidhdakakiaiaiaiaja
    r'[a-zA-Z0-9_\.+\-]+@[a-zA-Z0-9\-]+\.[a-zA-Z0-9-\.]+[\s|>>:\-/]*(Apple Password|Apple password|Password|password|pass|pw|PW|Pass|Pw)+[\s\.]*([-:>=]|[\s])+[\w!@#$%^&*()\-+=/?\.,\'\"~\\<>|]+': 'email/pass combo 1', 
    
    
    
    #Catches most email/password combinations...
    #Example: CUENTA: Ali436751@gmail.com:Hashimi12 
    r'\b([a-zA-Z0-9\.+\-=/?,!#$%^&*()]+@[a-zA-Z0-9]+\.[a-zA-Z0-9]+[\.a-zA-Z]*[\s]*(->|[|\:])+[\s]*[a-zA-Z0-9!~@#$%^&*()_\-+/=/?:<>\.,\|]+)\b': 'email/pass combo 2', 
    
    
    
    
    
    #Useful for email/pass combos that look like urls
    r'\b((username|Username|USERNAME|EMAIL|Email|user|User|ID|id|E-Mail|E-MAIL|e-mail|Mail|MAIL|mail)+[\s]*[\-=:]+[\s]*[a-zA-Z0-9!@#$%^&*()_+=\-\.,?]+[\-=&:\s]+(Password|password|pass|pw|PW|Pass|Pw)+[\s\.]*[:\->=]+[\s]*[a-zA-Z0-9\-_!@#$%^&*().,?]+)\b': 'email/pass combo 3',
    
    
    
    
    #No labeling at all
    #Example: OliverLatch305@messdev.xyz n8uXTiC0Qs
     r'[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+\s+[a-zA-Z0-9\-_!#$%^&*.,?]+[\n]+': 'email/pass combo 4', 
#     r'[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+\s+[a-zA-Z0-9\-_!#$%^&*.,?]+[\n]+': 'email/pass combo 4', 
    
    
    
    #RIGHT NOW set up to not catch anything
    #If we want to add a regex, we can do it here and everything else is set up
    r'(sdakjfha23kc3nei23nca4djsf)+': 'email/pass combo 5',
}        



tags_for_ids = ['ssn-US', 'Belgium', 'Czech,Slovakia - Birth Number', \
                'Denmark', 'France', 'Germany-Steuer-ID','Hungary-Szam', \
                'Latvia', 'Sweden', 'Switzerland','Switzerland-2008','Brazil-CPF', \
               'Finland', 'Austria-ssPIN', 'Czech,Slovakia - COP', 'Europe', \
                'Germany-PK', 'Germany-VSNR,RVNR', 'Greece', 'Ireland','Italy', \
                'Spain', 'UK-NI']
tags_for_phones = ['phone-US']

def parse_pastebinapi_url(x):
    return x['url'].lower().strip().replace('api_scrape_item.php?i=','')

def get_full_text(x, datapipe):
    response = datapipe.get_response(x['task_id']).body
    soup = bs4.BeautifulSoup(response, 'html.parser')
    text = ' '.join(soup.findAll(text=True))
    return text

def build_keywords_cols(x, keywords):
    tags = []
    for keyword in keywords:
        if keyword in x['text'].lower():
            tags.extend([keyword])
    return pd.Series({'keywords':tags, 'keywords#':len(tags)})

def build_pii_cols(x):
    pii = gen_extra_shingles(x['text'])
    emails, ids, phones = sort_pii(pii)    
    return pd.Series({'email#':len(emails), 'ids#':len(ids), 'phone##':len(phones)})

def is_valid_email(addr):  # noqa
    addr_parts = addr.split('@', 2)
    if len(addr_parts) != 2:
        return False
    name, host = addr_parts
    host_parts = host.split('.')
    if len(host_parts) < 2:
        return False
    if name in ['-', '+']:
        return False
    return True

def find_matches(text, regex):
    """Find all matches of 'regex' in 'text'
    Args:
        text (str): The text to search in.
        regex (regex): regex to match against.
    Yields:
        (str): All matches
    """
    current_pos = 0
    while current_pos <= len(text):
        match = re.compile(regex).search(text[current_pos:])
        if match is None:
            break
        current_pos += match.end()
        yield match.group(0)

def gen_extra_shingles(parsed_text):
    """Extra shingles are special shingles generated if a special case is
    detected.
    Args:
        parsed_text (str): The parsed page contents.
    Returns:
        extra_shingles (list): The extra shingles in plaintext
    """
    extra_shingles = []
    
    for pattern in regexes:
        for match in find_matches(parsed_text, pattern):
            tag = regexes[pattern]
            if tag in ('email',):
                if is_valid_email(match):
                    extra_shingles.append((tag, match.upper()))
            else:
                extra_shingles.append((tag, match))
    if (parsed_text.count('\n') == 1):
        for match in find_matches(parsed_text, r'[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+\s+[a-zA-Z0-9\-_!#$%^&*.,?]+'):
            tag = 'email/pass combo 5'
            extra_shingles.append((tag, match)) 
    return extra_shingles

def sort_pii(pii):
    emails = []
    combo_1 = []
    combo_2 = []
    combo_3 = []
    combo_4 = []
    combo_5 = []
    for item in pii:
        tag = item[0]
        if check_validity(item):
            if tag in ('email',):
                emails.append(item[1])
            elif tag in ('email/pass combo 1',):
                combo_1.append(item)
            elif tag in ('email/pass combo 2',):
                combo_2.append(item)
            elif tag in ('email/pass combo 3',):
                combo_3.append(item) 
            elif tag in ('email/pass combo 4',):
                combo_4.append(item)   
            elif tag in ('email/pass combo 5',):
                combo_5.append(item)
    emails = dedupe(emails) 
    combo_1 = dedupe(combo_1)
    combo_2 = dedupe(combo_2)
    combo_3 = dedupe(combo_3)
    combo_4 = dedupe(combo_4)
    combo_5 = dedupe(combo_5)
    return emails, combo_1, combo_2, combo_3, combo_4, combo_5

def dedupe(x):
    return list(set(x))

def read_list_from_file(filename):
    with open(filename,'r') as f:
        lines = f.read().splitlines()
    return lines

def get_breaches(email):
    emailhash = hashlib.sha1(email).hexdigest().upper()
    breaches = []
    filenames = [f for f in os.listdir('./resources/MoreHashedBreaches') if not f.startswith('.')]
    for filename in filenames:
        temp = read_list_from_file('./resources/MoreHashedBreaches/'+filename)
        if emailhash in temp:
            breaches.append(filename[:-4])
    return breaches


# https://alexwlchan.net/2018/01/listing-s3-keys-redux/
def get_matching_s3_objects(bucket, prefix='', suffix=''):
    """
    Generate objects in an S3 bucket.

    :param bucket: Name of the S3 bucket.
    :param prefix: Only fetch objects whose key starts with
        this prefix (optional).
    :param suffix: Only fetch objects whose keys end with
        this suffix (optional).
    """
    s3 = boto3.client('s3')
    kwargs = {'Bucket': bucket}

    # If the prefix is a single string (not a tuple of strings), we can
    # do the filtering directly in the S3 API.
    if isinstance(prefix, str):
        kwargs['Prefix'] = prefix

    while True:

        # The S3 API response is a large blob of metadata.
        # 'Contents' contains information about the listed objects.
        resp = s3.list_objects_v2(**kwargs)

        try:
            contents = resp['Contents']
        except KeyError:
            return

        for obj in contents:
            key = obj['Key']
            if key.startswith(prefix) and key.endswith(suffix):
                yield obj

        # The S3 API is paginated, returning up to 1000 keys at a time.
        # Pass the continuation token into the next response, until we
        # reach the final page (when this field is missing).
        try:
            kwargs['ContinuationToken'] = resp['NextContinuationToken']
        except KeyError:
            break


def get_matching_s3_keys(bucket, prefix='', suffix=''):
    """
    Generate the keys in an S3 bucket.

    :param bucket: Name of the S3 bucket.
    :param prefix: Only fetch keys that start with this prefix (optional).
    :param suffix: Only fetch keys that end with this suffix (optional).
    """
    for obj in get_matching_s3_objects(bucket, prefix, suffix):
        yield obj['Key']

#Finds and returns the longest of 3 lists        
def find_longest_list(list1, list2, list3, list4, list5):
    max_length = max(len(list1), len(list2), len(list3), len(list4), len(list5))
    if max_length == len(list1):
        return list1, 'combo_1'
    elif max_length == len(list2):
        return list2, 'combo_2'
    elif max_length == len(list3):
        return list3, 'combo_3'
    elif max_length == len(list4):
        return list4, 'combo_4'
    elif max_length == len(list5):
        return list5, 'combo_5'
    
def standardize_cred(creds):
    split_creds_list = re.split(r'(^Email|^Mail|^EMAIL|^E-mail|^E-Mail|^E-MAIL|^mail|^username|^USERNAME|^User|^user|^USER|Password|password|pass|pw.|pw|PW|PW.|Pass|pass.|Pass.|Pass :-|Pass:-|Pass-|Pass -|Password-|pass :-|pass:-|pass-|pass -|password-|[\s:&|><=])+', creds, 2)
    remove_list = ['\s', ':', '&', '|', '<', '>', '=', '', ' ', '-', '\n']
    split_creds_list = [i for i in split_creds_list if i not in remove_list]
    return split_creds_list

#     print('creds ' + creds)
#     no_hyphens_regex = r'(^Email|^Mail|^EMAIL|^E-mail|^E-Mail|^E-MAIL|^mail|^username|^USERNAME|^User|^user|^USER|Password|password|pass|pw.|pw|PW|PW.|Pass|pass.|Pass.|[-\s:&|><=])+'
#     test = re.split(no_hyphens_regex, creds, 2)
#     if len(test) > 2:
#         no_hyphens = False
#     else:
#         no_hyphens = True
#     hyphens_regex = r'(^Email|^Mail|^EMAIL|^E-mail|^E-Mail|^E-MAIL|^mail|^username|^USERNAME|^User|^user|^USER|Password|password|pass|pw.|pw|PW|PW.|Pass|pass.|Pass.|[\s:&|><=])+'
#     if no_hyphens:
#         split_creds_list = re.split(no_hyphens_regex, creds, 2)
#     else:
#         split_creds_list = re.split(hyphens_regex, creds, 2)
#     if no_hyphens:
#         remove_list = ['\s', ':', '&', '|', '<', '>', '=', '', ' ', '-']
#     else:
#         remove_list = ['\s', ':', '&', '|', '<', '>', '=', '', ' ']
#     split_creds_list = [i for i in split_creds_list if i not in remove_list]     
#     return split_creds_list

#checks validy of a match
def check_validity(match):
    creds = match[1]
    split_creds = standardize_cred(creds)
    try:
        email = split_creds[0]
        password = split_creds[1]
        if len(password) <=3:
            return False
        invalid_passwords = ['request', 'phone', 'failed', 'unit', 'main', 'date', 'whatsapp', 'instagram', 'twitter']
        if password.lower() in invalid_passwords:
            print('Invalid Password: ' + password)
            return False
        if re.search(r'\b([a-zA-Z0-9_\.+\-]+@[a-zA-Z0-9\-]+\.[a-zA-Z0-9-.]+)\b', password):
            print('Invalid Password(Email): ' + password)
            return False
        #Checks to see is password is a phone number
        if re.search(r'\(?[0-9]{3}[ \.\-:)]+[0-9]{3}[ \.\-:)]+[0-9]{4}', password):
            print('Invalid Password(Phone Number): ' + password)
            return False
        if email.endswith('>'):
            print('Invalid Email: ' + email)
            return False
        if password[len(password - 4)] == '.':
            print('Invalid Password(URL): ' + password)
            return False
    except:
        return True
    return True
    
def replace_other_lang(text, words):
    translator = Translator()
    for word in words:
        translated_word = translator.translate(word).text
        text = text.replace(word.lower(), str(translated_word))
        text = text.replace(word.upper(), str(translated_word))
        text = text.replace(word, str(translated_word))
    return text

def replace_commas(data_frame):
    return data_frame.replace(',', ':')
