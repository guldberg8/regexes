#Import libraries and datapipe
import pymysql
import pandas as pd
import numpy as np
import datetime as dt
import bs4
import re
import datapipe_helpers as dh
import urllib
import terbium_datapipe
import string


from urllib.parse import urlparse
from langdetect import detect
from IPython.display import clear_output
from googletrans import Translator
from urllib.parse import urlparse

import nltk
from nltk.tokenize import word_tokenize
from nltk.corpus import stopwords
stop_words = set(stopwords.words('english'))


# To get just the processed text for training data after getting type
def replace_urls(text):
    try:
        lambda text: re.sub(r'http[s]?://(?:[a-zA-Z]|[0-9]|[$-_@.&+]|[!*\(\), ]|(?:%[0-9a-fA-F][0-9a-fA-F]))+', lambda match: urlparse(match.group()).hostname, text).lower()
        return text
    except:
        lambda text: re.sub(r'http[s]?://(?:[a-zA-Z]|[0-9]|[$-_@.&+]|[!*\(\), ]|(?:%[0-9a-fA-F][0-9a-fA-F]))+', 'some_url ', text).lower()
        return text

def get_proc_text(datapipe, uuid):

    response = datapipe.get_response(uuid)
    contents = response.as_dict

    soup = bs4.BeautifulSoup(contents['body'], 'html.parser')
    parsed_text = ' '.join(soup.findAll(text=True))
    
    parsed_text = re.sub(' +', ' ',re.sub(r'[^@ \w\.]', ' ', parsed_text))

    try:
        language = detect(parsed_text)
    except:
        language = 'en'

    if not language=='en':
        translator = Translator()
        if len(parsed_text)>5000:
            try:
                translation = translator.translate(parsed_text[0:5000])
                parsed_text = translation.text
            except:
                parsed_text = parsed_text
        else:
            try:   
                translation = translator.translate(parsed_text)
                parsed_text = translation.text
            except:
                parsed_text = parsed_text
    
    parsed_text = replace_urls(parsed_text)
    parsed_text = parsed_text.lower()
    parsed_text = parsed_text.translate(str.maketrans('','','0123456789'))
    parsed_text = re.sub(' +', ' ', re.sub(r"[,_.;@#?!&$]+\ *", " ", parsed_text))
    parsed_text = word_tokenize(parsed_text)
    parsed_text=[word for word in parsed_text if word not in stop_words]
    parsed_text = ' '.join(parsed_text)
    return parsed_text

#Function for prod to get all vars
def get_prod_vars(datapipe, uuid):
    tags = []
    #print(datapipe_key.uuid)
    keywords = dh.read_list_from_file('./keywords.txt') 
    priority_keywords = ['doxx', 'hacked', 'cracked', 'fullz', 'dox', 'hack', 'crack', 'combolist']

    try:
        response = datapipe.get_response(uuid)
        contents = response.as_dict      
    except:
        print("datapipe response failed")

    soup = bs4.BeautifulSoup(contents['body'], 'html.parser')
    parsed_text = ' '.join(soup.findAll(text=True))
    page_urls = re.findall(r'http[s]?://(?:[a-zA-Z]|[0-9]|[$-_@.&+]|[!*\(\), ]|(?:%[0-9a-fA-F][0-9a-fA-F]))+', parsed_text) 
    parsed_text = re.sub(' +', ' ',re.sub(r'[^@ \w\.]', ' ', parsed_text))

    try:
        language = detect(parsed_text)
    except:
        language = 'en'

    if not language=='en':
        translator = Translator()
        if len(parsed_text)>5000:
            try:
                translation = translator.translate(parsed_text[0:5000])
                parsed_text = translation.text
            except:
                parsed_text = parsed_text
        else:
            try:   
                translation = translator.translate(parsed_text)
                parsed_text = translation.text
            except:
                parsed_text = parsed_text
    
    parsed_text = replace_urls(parsed_text)
    parsed_text = parsed_text.lower()
    parsed_text = parsed_text.translate(str.maketrans('','','0123456789'))
    parsed_text = re.sub(' +', ' ', re.sub(r"[,_.;@#?!&$]+\ *", " ", parsed_text))
    parsed_text = word_tokenize(parsed_text)
    parsed_text=[word for word in parsed_text if word not in stop_words]
    parsed_text = ' '.join(parsed_text)
    
    parsed_url = contents['url'].replace('api_scrape_item.php?i=','')
       
    #generate features
    for keyword in keywords:
        if keyword in contents['body']:
            tags.extend([keyword])

    pii = dh.gen_extra_shingles(contents['body'])
    emails, ids, phones = dh.sort_pii(pii)     

    p_keywords = [x for x in tags if x in priority_keywords]
    df = pd.DataFrame([{'url': parsed_url, 
                        'AllKeywords#': len(tags),
                        'AllKeywords': tags,
                        'PriorityKeywords#': len(p_keywords),
                        'PriorityKeywords': p_keywords,
                        'Emails#': len(emails),
                        'Emails': emails,
                        'IDs#': len(ids),
                        'IDs': ids,
                        'Phones#': len(phones),
                        'Phones': phones,
                        'Page_urls': page_urls,
                        'Page_urls#': len(page_urls),
                        'proc_text': parsed_text
                      }], 
                       index=[contents['uuid']])
    
    df['words#'] = df['proc_text'].str.split().str.len()
    return df